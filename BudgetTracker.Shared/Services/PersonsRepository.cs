﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BudgetTracker.Shared.Exceptions;
using BudgetTracker.Shared.Models;

namespace BudgetTracker.Shared.Services
{
    public class PersonsRepository : IPersonsRepository
    {
        private List<Person> persons = new List<Person>();

        public void Add(Person person)
        {
            try
            {
                person.Id = AssignId();

                persons.Add(person);
            }
            catch(Exception ex)
            {
                throw new PersonAddException(ex.Message);
            }
        }

        public IEnumerable<Person> Get()
        {
            return persons;
        }

        public Person GetById(int id)
        {
            return persons.Where(p => p.Id == id).FirstOrDefault();
        }

        public void Remove(Person person)
        {
            try
            {
                persons.Remove(person);
            }
            catch(Exception ex)
            {
                throw new PersonRemoveException(ex.Message);
            }
        }

        public void Update(Person person)
        {
            try
            {
                var p = GetById(person.Id);

                p = person;
            }
            catch(Exception ex)
            {
                throw new PersonUpdateException(ex.Message);
            }
        }

        private int AssignId()
        {
            var id = 0;

            if(persons.Count > 0)
            {
                id = persons.Max(p => p.Id) + 1;
            }

            return id;
        }

        public void GenerateSampleData()
        {
            Add(new Person()
            {
                FirstName = "Mateusz",
                LastName = "Kocur",
                Email = "93mateuszkocur@gmail.com"
            });

            Add(new Person()
            {
                FirstName = "Sylwia",
                LastName = "Morańska",
                Email = "sylwiamoranska@gmail.com"
            });
        }
    }
}
