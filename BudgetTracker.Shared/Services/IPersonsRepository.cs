﻿using BudgetTracker.Shared.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BudgetTracker.Shared.Services
{
    interface IPersonsRepository
    {
        IEnumerable<Person> Get();

        Person GetById(int id);

        void Add(Person person);

        void Update(Person person);

        void Remove(Person person);
    }
}
