﻿using System;

namespace BudgetTracker.Shared.Exceptions
{
    public class PersonAddException : Exception
    {
        public PersonAddException(string message) : base(message)
        {
        }
    }
}
