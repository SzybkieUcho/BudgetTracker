﻿using System;

namespace BudgetTracker.Shared.Exceptions
{
    public class PersonUpdateException : Exception
    {
        public PersonUpdateException(string message) : base(message)
        {
        }
    }
}
