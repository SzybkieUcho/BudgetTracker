﻿using System;

namespace BudgetTracker.Shared.Exceptions
{
    public class PersonRemoveException : Exception
    {
        public PersonRemoveException(string message) : base(message)
        {
        }
    }
}
